
package institute;

import java.util.ArrayList;
import java.util.List;


public class InsDepStu {

   
    public static void main(String[] args) {
       //create students
       Student st1 = new Student("Ash", 111);
       Student st2 = new Student("Tim", 222);
       Student st3 = new Student("Leo", 333);
       Student st4 = new Student("Graham", 444);
       Student st5 = new Student("Courtney", 555);
       Student st6 = new Student("Lisa", 666);
       
       //add 3 students to D1 list
       List<Student> studentsD1 = new ArrayList<>();
       studentsD1.add(st1);
       studentsD1.add(st2);
       studentsD1.add(st3);
       //add 3 students to D2 list
       List <Student> studentsD2 = new ArrayList<>();
       studentsD2.add(st4);
       studentsD2.add(st5);
       studentsD2.add(st6);
       
       //create departments, add created student arrays
       Department D1 = new Department("Department 1", studentsD1);
       Department D2 = new Department("Department 2", studentsD2);
       
       //add departments into an array of departments
       List<Department> departments = new ArrayList<>();
       departments.add(D1);
       departments.add(D2);
       
       //Make institute, add departments into institute
       Institute institute = new Institute("Institute", departments);
       
       System.out.println("There are " + institute.getStudentsInInstitute() 
                        + " students currently attending this institute.");
       
       
    }
    
}
