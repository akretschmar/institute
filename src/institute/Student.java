
package institute;


public class Student {
    private String name;
    private int id;
    
    public Student(String name, int id){
        this.name = name;
        this.id = id;
    }
    
    public String getStudentName(){
        return this.name;
    }
    public int getStudentId(){
        return this.id;
    }
   
}
