
package institute;

import java.util.List;


public class Institute {
    private String name;
    private List<Department> departments;
    
    public Institute(String name, List<Department> departments){
        this.name = name;
        this.departments = departments;
    }
    
    public List<Department> getDepartments(){
        return departments;
    }
    
    public int getStudentsInInstitute()
    {
        int noOfStudents = 0;
        List<Student> students;
        for(Department dept: departments) //dataType item : array
        {
            students = dept.getStudents();
            for(Student s: students)
            {
                noOfStudents++;
            }
        }
        return noOfStudents;
    }
    
}
